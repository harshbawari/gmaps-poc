import React, { useRef, useEffect, useState } from 'react';
import { 
  View, 
  Text, 
  StyleSheet,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  PermissionsAndroid,
  Platform,
  Image
} from 'react-native';
import { Header, Icon } from 'react-native-elements';
import GetLocation from 'react-native-get-location';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'; 
import MapViewDirections from 'react-native-maps-directions';


function App() {

  const map = useRef();

  const [loading, setLoading] = useState(true);
  const [showMarker, setShowMarker] = useState(false);

  const [region, setRegion] = useState({
    latitude: 22,
    longitude: 77,
    latitudeDelta: 0,
    longitudeDelta: 0
  });

  const [officeLoc, setOfficeLoc] = useState({
    latitude: 28.4212234,
    longitude: 77.0382461
  });

  const [clgLoc, setClgLoc] = useState({
    latitude: 30.2690207,
    longitude: 78.0421013
  });

  const get_permissions = async () => {
    console.log('getting permissions');
    if(Platform.OS === 'android') {
      console.log('OS: Android');
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Hello'
        }
      );
    }
  }

  useEffect(() => {
    get_permissions();
  }, []);

  const get_location = async () => {
    console.log('gettinglocation');
    
    GetLocation.getCurrentPosition({
      timeout: 10000
    })
    .then((location) => {
      console.log('Location: ', location);
      setRegion({
        latitude: location.latitude,
        longitude: location.longitude,
        latitudeDelta: 0.015,
        longitudeDelta: 0.03
      });
    })
    .then(() => {
      setLoading(false);
      setShowMarker(true);
    })
    .catch(err => {
      console.log('Error in getting location: ', err);
    })
    .finally(async () => {
      const dest = { latitude: region.latitude, longitude: region.longitude };
      map.current.fitToSuppliedMarkers(['originMarker', 'destMarker'], {
        animated: true,
        edgepadding: {
          top: 150,
          bottom: 50,
          right: 50,
          left: 50
        }
      });
    });
  }

  const onRegionChange = (region) => {
    setRegion(region);
  }

  const GOOGLE_MAPS_APIKEY = 'AIzaSyBdvRn4ztDhgJaWiOhxEQAoIbIpT7CsK7w'

  return (
    <SafeAreaView style={{ flex: 1 }}> 
      <StatusBar barStyle={Platform.OS === 'android' ? 'light-content' : 'dark-content'} />
      <SafeAreaView style={{ flex: 1, backgroundColor: 'red', padding: '2%'}}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          region={region}
          ref={map}
        >
          {!loading && showMarker
            ? (
              <>
                <MapViewDirections
                  origin={clgLoc}
                  destination={{
                    latitude: region.latitude,
                    longitude: region.longitude
                  }}
                  apikey={GOOGLE_MAPS_APIKEY}
                  strokeWidth={5}
                  strokeColor='blue'
                  onReady={result => {
                    console.log('Distance: ', result.distance);
                  }}
                />
                <Marker
                  key={0}
                  coordinate={{
                    latitude: region.latitude,
                    longitude: region.longitude
                  }}
                  title='Your are here!'
                  identifier='originMarker'
                >
                  <View>
                    <Image
                      source={require('./src/assets/images/home.png')}
                      style={{
                        height: 100,
                        width: 60
                      }}
                      resizeMode='contain'
                    /> 
                  </View>
                </Marker>
                <Marker
                  key={1}
                  coordinate={{
                    latitude: clgLoc.latitude,
                    longitude: clgLoc.longitude
                  }}
                  title='College Location'
                  identifier='destMarker'
                >
                  <View>
                    <Image
                      source={require('./src/assets/images/clg.png')}
                      style={{
                        height: 100,
                        width: 60
                      }}
                      resizeMode='contain'
                    /> 
                  </View>
                </Marker>
              </>
            )  : (
              null
            )
          }
        </MapView>
        <TouchableOpacity 
          style={{ backgroundColor: 'white', borderRadius: 50, position: 'absolute', bottom: 50, right: 20, padding: '4%', alignItems: 'center', justifyContent: 'center'}}
          onPress={get_location}
        >
          <View>
            <Icon
              name='location'
              type='entypo'
              size={30}
            />
          </View>
        </TouchableOpacity>
      </SafeAreaView>
    </SafeAreaView>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject
  }
 });
